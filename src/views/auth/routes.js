export default [
  // 登陆路由
  {
    path: '/auth/login',
    name: 'Login',
    meta: {
      Login: false,
    },
    component: () => import('./Login')
  },
  //注册路由
  {
    path: '/auth/register',
    name: 'Register',
    meta: {
      login: false,
    },
    component: () => import('./Register')
  }
]