export default [
    {
      path: '/personal-center/student',
      name: 'StudentPersonalCenter',
      meta: { login: true },
      component: () => import('./Student')
    },
    {
      path: '/personal-center/teacher',
      name: 'TeacherPersonalCenter',
      meta: { login: true },
      component: () => import('./Teacher')
    }
  ]