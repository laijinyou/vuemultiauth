import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import plugin from './plugin'

Vue.use(Vuex)

// 创建 store 对象
const store = new Vuex.Store({
  modules: {
    auth,
  },
  plugins: [plugin]
})

// 导出 store 对象
export default store