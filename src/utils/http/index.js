import axios  from 'axios' // 基于promise的http库，拦截请求和响应、取消请求、转换json、客户端防御cSRF等
import { Message } from 'element-ui' // 常用于主动操作后的反馈提示,用于系统级通知的被动提醒。
import router from '../../router' // 引入路由

// 自定义 axios 实例添加拦截器，发送请求前可以进行一个请求的拦截，检测是否符合相应的权限操作
const httpRequest = axios.create({
  timeout: 10000, //设置默认的请求超时时间。例如超过了10s，就会告知用户当前请求超时，请刷新等。
  baseURL: process.env.VUE_APP_API_URL //设置请求地址
})

// 添加请求拦截器
httpRequest.interceptors.request.use(
  // 在发送请求之前做些什么
  // 每次发送请求之前判断vuex中是否存在token        
  // 如果存在，则统一在http请求的header都加上token，这样后台根据token判断你的登录情况
  // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断 
  config => {
    return config
  },
  // 对请求错误做些什么
  error => {
    return Promise.reject(error)
  }
)

// 导出带有token的 HTTP 请求头
export function setHttpToken(token) {
  httpRequest.defaults.headers.common.Authorization = `Bearer ${token}`
}

// 添加响应拦截器
httpRequest.interceptors.response.use(
  // 对响应数据做点什么
  // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据     
  // 否则的话抛出错误
  response => {
    return response
  },
  // 对响应错误做点什么
  // 服务器状态码不是2开头的的情况
  // 这里可以跟你们的后台开发人员协商好统一的错误状态码    
  // 然后根据返回的状态码进行一些操作，例如登录过期提示，错误提示等等
  error => {
    let message = error.response.data.message ? error.response.data.message : error.response.statusText
    let dangerouslyUseHTMLString = false
    
    // 错误代码 422
    if (error.response.status === 422 && error.response.data.hasOwnProperty('errors')) {
      message += '<br>';
      for (let key in error.response.data.errors) {
        let items = error.response.data.errors[key]
        if (typeof items === 'string') {
          message += `${items} <br>`
        } else {
          error.response.data.errors[key].forEach( item => {
            message += `${item} <br>`
          })
        }
      }
      dangerouslyUseHTMLString = true
    }
    // 错误代码 401
    if (error.response.status === 401 && error.response.data.message === 'Unauthenticated.') {
      router.push({name: 'authLogin'})
    }

    Message({
      dangerouslyUseHTMLString,
      message: message,
      type: 'error'
    })

    return Promise.reject(error)
  }
)

export default httpRequest